package com.dikhawk.realtrackmania.utils

import android.content.Context
import android.preference.PreferenceManager

/**
 * Created by dik on 18.06.16.
 */
class SharedPreferencesUtils(private val context: Context) {
    private val USER_NAME = "user_name"

    var userName: String?
        get() {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

            return sharedPref.getString(USER_NAME, null)
        }
        set(value) {
            val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = sharedPref.edit()
            editor.putString(USER_NAME, value)
            editor.commit()
        }
}