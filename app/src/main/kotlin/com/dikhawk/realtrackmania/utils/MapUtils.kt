package com.dikhawk.realtrackmania.utils

import android.graphics.Color
import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolylineOptions
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

/**
 * Created by dik on 19.06.16.
 */
class MapUtils {

    private constructor()

    companion object {
        fun getMaxAltitude(list: ArrayList<Location>): Double {
            var max: Double = list[0].altitude

            for (location in list) {
                if (max < location.altitude) max = location.altitude
            }

            return max
        }

        fun getMinAltitude(list: ArrayList<Location>): Double {
            var min: Double = list[0].altitude

            for (location in list) {
                if (min > location.altitude) min = location.altitude
            }

            return min
        }

        fun locationListToPolylineOptions(list: ArrayList<Location>): PolylineOptions {
            val polyOptions = PolylineOptions().width(5F).color(Color.BLUE).geodesic(true)
            for (location in list) {
                var latLng = LatLng(location.latitude, location.longitude)
                polyOptions.add(latLng)
            }

            return polyOptions
        }

        fun meterToKilometer(meters: Double): Double {
            var kilometers = meters / 1000

            return BigDecimal(kilometers).setScale(2, RoundingMode.UP).toDouble()
        }

        fun roundingValue(value: Double, point: Int): Double {
            return BigDecimal(value).setScale(point, RoundingMode.UP).toDouble()
        }
    }
}