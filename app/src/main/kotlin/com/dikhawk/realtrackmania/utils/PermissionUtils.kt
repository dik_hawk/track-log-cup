package com.dikhawk.realtrackmania.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat

/**
 * Created by dik on 11.07.16.
 */
class PermissionUtils() {

    companion object {
//        private var context: Context? = null

        fun isAccessFineLocation(context: Context): Boolean {
            return if (checkPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) true else false
        }

        fun isAccessCoarseLocation(context: Context): Boolean {
            return if (checkPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) true else false
        }

        private fun checkPermission(context: Context, permissionName: String): Int =
                ContextCompat.checkSelfPermission(context, permissionName)
    }
}