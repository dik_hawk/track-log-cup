package com.dikhawk.realtrackmania

import android.app.Application
import com.dikhawk.realtrackmania.di.AndroidModule
import com.dikhawk.realtrackmania.di.AppComponent
import com.dikhawk.realtrackmania.di.DaggerAppComponent

/**
 * Created by dik on 13.05.16.
 */
class MyApplication : Application() {

    companion object {
        @JvmStatic lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent
                .builder()
                .androidModule(AndroidModule(this))
                .build()

        component.getOrmHelper().writableDatabase
    }
}