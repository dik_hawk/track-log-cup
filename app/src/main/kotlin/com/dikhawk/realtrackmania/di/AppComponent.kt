package com.dikhawk.realtrackmania.di

import com.dikhawk.realtrackmania.ui.activities.RegistrationActivity
import com.dikhawk.realtrackmania.ui.fragments.GoogleMapFragment
import com.dikhawk.realtrackmania.ui.fragments.MyTracksFragment
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import dagger.Component
import javax.inject.Singleton

/**
 * Created by dik on 29.08.16.
 */
@Singleton
@Component(modules = arrayOf(AndroidModule::class, DbModule::class))
interface AppComponent {
    fun inject(f: GoogleMapFragment)
    fun inject(f: MyTracksFragment)
    fun inject(activity: RegistrationActivity)

    fun getOrmHelper(): OrmLiteSqliteOpenHelper
}