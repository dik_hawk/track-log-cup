package com.dikhawk.realtrackmania.di

import android.content.Context
import com.dikhawk.realtrackmania.data.db.TrackMarksDAO
import com.dikhawk.realtrackmania.data.db.TrackResultsDAO
import com.dikhawk.realtrackmania.data.db.TracksDAO
import com.dikhawk.realtrackmania.data.db.UsersDAO
import com.dikhawk.realtrackmania.data.db.helper.ORMLiteHelper
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.dikhawk.realtrackmania.data.db.model.TrackResult
import com.dikhawk.realtrackmania.data.db.model.User
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by dik on 29.08.16.
 */
@Module
class DbModule {

    @Singleton
    @Provides
    fun provideOrmHelper(context: Context): OrmLiteSqliteOpenHelper {
        return OpenHelperManager.getHelper(context, ORMLiteHelper::class.java)
    }

    @Singleton
    @Provides
    fun provideUsersDAO(ormHelper: OrmLiteSqliteOpenHelper): UsersDAO {
        return UsersDAO(ormHelper.connectionSource, User::class.java)
    }

    @Singleton
    @Provides
    fun provideTracksDAO(ormHelper: OrmLiteSqliteOpenHelper): TracksDAO {
        return TracksDAO(ormHelper.connectionSource, Track::class.java)
    }

    @Singleton
    @Provides
    fun provideTrackMarksDAO(ormHelper: OrmLiteSqliteOpenHelper): TrackMarksDAO {
        return TrackMarksDAO(ormHelper.connectionSource, TrackMark::class.java)
    }

    @Singleton
    @Provides
    fun provideTrackResultsDAO(ormHelper: OrmLiteSqliteOpenHelper): TrackResultsDAO {
        return TrackResultsDAO(ormHelper.connectionSource, TrackResult::class.java)
    }
}