package com.dikhawk.realtrackmania.ui.validators

import android.content.Context
import android.widget.EditText
import com.dikhawk.realtrackmania.R

/**
 * Created by dik on 16.06.16.
 */
class EditTextValidator {

    companion object {
        fun standardValidation(context: Context, textView: EditText): Boolean {
            var text: String = textView.text.toString()

            if (text.equals("")) {
                textView.error = context.getText(R.string.error_empty_field)
                return false
            }

            return true
        }
    }
}