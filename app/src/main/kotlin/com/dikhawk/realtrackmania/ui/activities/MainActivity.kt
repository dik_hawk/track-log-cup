package com.dikhawk.realtrackmania.ui.activities

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.ui.fragments.TracksPagerFragment

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var mDrawer: DrawerLayout? = null
    private var navigationView: NavigationView? = null
    private var mToggle: ActionBarDrawerToggle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDrawer = findViewById(R.id.drawer_layout) as DrawerLayout?
        navigationView = findViewById(R.id.nav_view) as NavigationView?

        navigationView?.setCheckedItem(R.id.nav_map)

        if (savedInstanceState == null) {
            navigationView?.menu?.getItem(0)?.isChecked = true
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.fragment_container, TracksPagerFragment())
            ft.commit()
        }
    }

    override fun setSupportActionBar(toolbar: Toolbar?) {
        if (mToggle != null) mDrawer?.removeDrawerListener(mToggle as ActionBarDrawerToggle)

        mToggle = ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        mDrawer?.addDrawerListener(mToggle as ActionBarDrawerToggle)
        mToggle?.syncState()

        navigationView?.setNavigationItemSelectedListener(this)

        super.setSupportActionBar(toolbar)
    }

    override fun onBackPressed() {
        if (mDrawer?.isDrawerOpen(GravityCompat.START) as Boolean) {
            mDrawer?.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId

        if (id == android.R.id.home) {
            mDrawer?.openDrawer(GravityCompat.START)  // OPEN DRAWER
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val ft = supportFragmentManager.beginTransaction()

        if (id == R.id.nav_track_list) {
            ft.replace(R.id.fragment_container, TracksPagerFragment())
        } else if (id == R.id.nav_map) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        ft.commit()

        mDrawer?.closeDrawer(GravityCompat.START)
        return true
    }
}
