package com.dikhawk.realtrackmania.ui.fragments

/**
 * Created by dik on 14.06.16.
 */
interface IFragmentTitle {
    fun getTitle(): Int
}