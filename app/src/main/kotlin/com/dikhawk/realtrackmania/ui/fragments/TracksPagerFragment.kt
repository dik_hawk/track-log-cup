package com.dikhawk.realtrackmania.ui.fragments


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.ui.activities.BaseActivity
import com.dikhawk.realtrackmania.ui.adapters.TrackListPagerAdapter
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class TracksPagerFragment : Fragment() {

    private var mContainer: ViewPager? = null
    private var mTabs: TabLayout? = null
    private var mPagerAdapter: FragmentPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_track_pager, container, false)

        mContainer = view.findViewById(R.id.container) as ViewPager
        mTabs = view.findViewById(R.id.tabs) as TabLayout

        (activity as BaseActivity).setSupportActionBar(view.findViewById(R.id.toolbar) as Toolbar)

        val fragments = ArrayList<Fragment>()
        fragments.add(MyTracksFragment())
        fragments.add(FavoriteFragment())
        fragments.add(MapTracksFragment())

        mPagerAdapter = TrackListPagerAdapter(childFragmentManager, context, fragments)
        mContainer?.adapter = mPagerAdapter
        mTabs?.setupWithViewPager(mContainer)

        return view
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
}
