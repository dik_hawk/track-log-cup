package com.dikhawk.realtrackmania.ui.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.dikhawk.realtrackmania.ui.fragments.IFragmentTitle

/**
 * Created by dik on 14.06.16.
 */
class TrackListPagerAdapter(var fm: FragmentManager?,
                            var context: Context,
                            var fragments: List<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(position: Int): Fragment? {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = (fragments[position] as IFragmentTitle).getTitle()
        return context.getText(title)
    }
}