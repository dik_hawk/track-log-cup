package com.dikhawk.realtrackmania.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dikhawk.realtrackmania.R

/**
 * A simple [Fragment] subclass.
 */
class MapTracksFragment : Fragment(), IFragmentTitle {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_map_tracks, container, false)
    }

    override fun getTitle(): Int {
        return R.string.tab_map_tracks
    }
}
