package com.dikhawk.realtrackmania.ui.fragments

import com.dikhawk.realtrackmania.data.db.model.TrackMark
import lecho.lib.hellocharts.model.PointValue
import java.util.*

/**
 * Created by dik on 05.07.16.
 */
class ChartFragmentFactory {
    companion object {
        fun getChartAltitude(list: List<TrackMark>, nameAxisX: String, nameAxisY: String): ChartFragment {
            var points = ArrayList<PointValue>()

            for (i in 0..list.size - 1) {
                points.add(PointValue(i.toFloat(), list[i].altitude?.toFloat() as Float))
            }

            val fragment = ChartFragment.newInstance(points, nameAxisX, nameAxisY)

            return fragment
        }
    }
}