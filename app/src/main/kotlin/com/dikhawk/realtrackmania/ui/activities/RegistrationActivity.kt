package com.dikhawk.realtrackmania.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.dikhawk.realtrackmania.MyApplication
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.data.db.UsersDAO
import com.dikhawk.realtrackmania.data.db.model.User
import com.dikhawk.realtrackmania.ui.validators.EditTextValidator
import com.dikhawk.realtrackmania.utils.SharedPreferencesUtils
import javax.inject.Inject

class RegistrationActivity : BaseActivity() {

    var mUserName: EditText? = null
    var mToRegister: Button? = null
    var spUtils: SharedPreferencesUtils? = null
    @Inject
    lateinit var usersDAO: UsersDAO

    override fun onCreate(savedInstanceState: Bundle?) {
        MyApplication.component.inject(this)
        spUtils = SharedPreferencesUtils(applicationContext)

        if (spUtils?.userName != null) {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        initViews()
    }

    private fun initViews() {
        mUserName = findViewById(R.id.et_user_name) as EditText
        mToRegister = findViewById(R.id.bt_to_register) as Button

        mToRegister?.setOnClickListener(registerListener)
    }

    val registerListener = View.OnClickListener {
        val isValid = EditTextValidator.standardValidation(applicationContext, mUserName as EditText)
        if (isValid) {
            var user = User()

            user.userName = mUserName?.text.toString()
            usersDAO.save(user)

            spUtils?.userName = mUserName?.text.toString()
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }
    }
}
