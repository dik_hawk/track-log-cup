package com.dikhawk.realtrackmania.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.utils.MapUtils

/**
 * Created by dik on 18.06.16.
 */
class TrackListAdapter(val list: List<Track>,
                       val listener: TrackItemListener) : RecyclerView.Adapter<TrackListAdapter.ViewHolder>() {

    private var context: Context? = null

    override fun onBindViewHolder(holder: TrackListAdapter.ViewHolder?, position: Int) {
        val track = list[position]
        val distance = MapUtils.meterToKilometer(track.distance as Double).toString()
        val minAltitude = MapUtils.roundingValue(track.minAltitude as Double, 2)
        val maxAltitude = MapUtils.roundingValue(track.maxAltitude as Double, 2)
        val strKm = context?.getString(R.string.item_track_kilometers)
        val strMet = context?.getString(R.string.item_track_meter)

        holder?.menuButton?.setOnClickListener({ view -> listener.onClickMenuButton(view) })
        holder?.distance?.text = "$distance $strKm"
        holder?.altitude?.text = "$minAltitude-$maxAltitude $strMet"
        holder?.trackTitle?.text = track.title
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_track, parent, false)
        context = parent?.context
        view?.setOnClickListener({ view -> listener.onClickItem(view) })

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var trackTitle: TextView
        var distance: TextView
        var altitude: TextView
        var menuButton: ImageButton

        init {
            trackTitle = itemView?.findViewById(R.id.tv_track_title) as TextView
            distance = itemView?.findViewById(R.id.tv_distance) as TextView
            altitude = itemView?.findViewById(R.id.tv_altitude) as TextView
            menuButton = itemView?.findViewById(R.id.item_menu_button) as ImageButton
        }
    }

    interface TrackItemListener {
        fun onClickMenuButton(view: View)
        fun onClickItem(view: View)
    }
}