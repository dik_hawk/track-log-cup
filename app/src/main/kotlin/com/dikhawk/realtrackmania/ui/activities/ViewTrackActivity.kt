package com.dikhawk.realtrackmania.ui.activities

import android.os.Bundle
import android.widget.TextView
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.dikhawk.realtrackmania.data.db.model.User
import com.dikhawk.realtrackmania.ui.fragments.ChartFragmentFactory
import com.dikhawk.realtrackmania.ui.fragments.GoogleMapFragment
import com.dikhawk.realtrackmania.utils.MapUtils
import java.util.*

class ViewTrackActivity : BaseActivity() {

    object ACTION {
        const val VIEW_TRACK = "view_track"
    }

    object VALUE_NAME {
        const val USER = "user"
        const val TRACK = "track"
        const val TRACK_MARKS = "track_marks"
    }

    private var mAuthor: TextView? = null
    private var mDistance: TextView? = null
    private var mAltitude: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_track)
        displayHomeUp(true)

        initViews()

        if (intent.action.equals(ViewTrackActivity.ACTION.VIEW_TRACK)) {
            var markList = intent.getSerializableExtra(VALUE_NAME.TRACK_MARKS) as ArrayList<TrackMark>

            initChartView(markList)

            val mapFragment = GoogleMapFragment.newInstance(markList, true)
//        mapFragment.setIGoogleMapFragment(iGoogleMapFragment)

            val ft = supportFragmentManager.beginTransaction()
            ft.add(R.id.map_container, mapFragment)
            ft.commit()
        }
    }

    fun initViews() {
        val user: User? = intent.getSerializableExtra(VALUE_NAME.USER) as User
        val track: Track? = intent.getSerializableExtra(VALUE_NAME.TRACK) as Track
        val minAltitude = MapUtils.roundingValue(track?.minAltitude as Double, 2)
        val maxAltitude = MapUtils.roundingValue(track?.maxAltitude as Double, 2)
        val meters = getString(R.string.view_track_meters)
        val kilometers = getString(R.string.view_track_kilometers)

        mAuthor = findViewById(R.id.tv_author) as TextView
        mDistance = findViewById(R.id.tv_distance) as TextView
        mAltitude = findViewById(R.id.tv_altitude) as TextView

        mAuthor?.text = user?.userName
        mDistance?.text = "${MapUtils.meterToKilometer(track?.distance as Double)} $kilometers"
        mAltitude?.text = "${minAltitude}-${maxAltitude} $meters"
    }

    private fun initChartView(markList: ArrayList<TrackMark>) {
        val nameX: String = getString(R.string.chart_altitude_axisX_name)
        val nameY: String = getString(R.string.chart_altitude_axisY_name)

        supportFragmentManager.beginTransaction()
                .add(R.id.chart_container,
                        ChartFragmentFactory.getChartAltitude(markList, nameX, nameY)).commit()
    }
}
