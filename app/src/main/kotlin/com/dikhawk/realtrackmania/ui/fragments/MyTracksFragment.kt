package com.dikhawk.realtrackmania.ui.fragments


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dikhawk.realtrackmania.MyApplication
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.data.db.TracksDAO
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.ui.activities.AddEditTrackActivity
import com.dikhawk.realtrackmania.ui.activities.ViewTrackActivity
import com.dikhawk.realtrackmania.ui.adapters.TrackListAdapter
import java.util.*
import javax.inject.Inject

/**
 * A simple [Fragment] subclass.
 */
class MyTracksFragment : Fragment(), IFragmentTitle, View.OnClickListener {

    private var mAddTrack: FloatingActionButton? = null
    private var mTracks: RecyclerView? = null
    private var mTracksLayoutManager: StaggeredGridLayoutManager? = null
    private var mAdapterTrack: TrackListAdapter? = null
    private var mTrackList: ArrayList<Track>? = null
    @Inject
    lateinit var mTracksDAO: TracksDAO
    private val REQUEST_CODE_VIEW_TRACK = 1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_my_tracks, container, false)

        MyApplication.component.inject(this)
        initViews(view)

        return view
    }

    private fun initViews(view: View) {
        mAddTrack = view.findViewById(R.id.fab_add_track) as FloatingActionButton
        mTracks = view.findViewById(R.id.rv_tracks) as RecyclerView

        mTracksLayoutManager = StaggeredGridLayoutManager(1, 1)
        mTrackList = ArrayList(mTracksDAO.getTracks())
        mAdapterTrack = TrackListAdapter(mTrackList as List<Track>, onClickListener)

        mAddTrack?.setOnClickListener(this)
        mTracks?.layoutManager = mTracksLayoutManager
        mTracks?.adapter = mAdapterTrack
    }

    private var onClickListener = object : TrackListAdapter.TrackItemListener {
        override fun onClickMenuButton(view: View) {
            throw UnsupportedOperationException()
        }

        override fun onClickItem(view: View) {
            val intent = Intent(activity.baseContext, ViewTrackActivity::class.java)
            val position = mTracks?.getChildLayoutPosition(view)
            val track: Track? = mTrackList?.get(position as Int)

            intent.action = ViewTrackActivity.ACTION.VIEW_TRACK

            intent.putExtra(ViewTrackActivity.VALUE_NAME.USER, track?.user)
            intent.putExtra(ViewTrackActivity.VALUE_NAME.TRACK, track)
            intent.putExtra(ViewTrackActivity.VALUE_NAME.TRACK_MARKS, ArrayList(track?.trackMarks))
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_VIEW_TRACK) {
            if (resultCode == Activity.RESULT_OK) {
                mTrackList?.clear()
                mTrackList?.addAll(ArrayList(mTracksDAO.getTracks()))
                mAdapterTrack?.notifyDataSetChanged()
            }
        }
    }

    override fun getTitle(): Int {
        return R.string.tab_my_tracks
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.fab_add_track) {
            val intent = Intent(context, AddEditTrackActivity::class.java)
            intent.putExtra(AddEditTrackActivity.INTENT_NAME.MODE,
                    AddEditTrackActivity.INTENT_VALUES.MODE_ADD)
            startActivityForResult(intent, REQUEST_CODE_VIEW_TRACK)
        }
    }
}