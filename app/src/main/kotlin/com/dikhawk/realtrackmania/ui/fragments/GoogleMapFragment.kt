package com.dikhawk.realtrackmania.ui.fragments

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.util.Log
import com.dikhawk.realtrackmania.MyApplication
import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.dikhawk.realtrackmania.utils.PermissionUtils
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.PolylineOptions
import java.util.*
import javax.inject.Inject

/**
 * Created by dik on 11.07.16.
 */
class GoogleMapFragment : SupportMapFragment() {

    @Inject
    lateinit var mGoogleApiClient: GoogleApiClient
    private val LOG_TAG = javaClass.simpleName
    private var iMapFragment: IMapFragment? = null
    private var mMap: GoogleMap? = null
    private var mOnlyMap: Boolean = false
    private val REQUEST_CODE_PERMISSION_LOCATION: Int = 1

    companion object {
        private val ARGS_MARKS = "marks"
        private val ARGS_ONLY_MAP = "only_map"

        fun newInstance(): GoogleMapFragment {
            val fragment = GoogleMapFragment()

            return fragment
        }

        fun newInstance(marks: ArrayList<TrackMark>, onlyMap: Boolean): GoogleMapFragment {
            val fragment = GoogleMapFragment()
            val args = Bundle()

            args.putSerializable(ARGS_MARKS, marks)
            args.putBoolean(ARGS_ONLY_MAP, onlyMap)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(p0: Bundle?) {
        super.onCreate(p0)

        MyApplication.component.inject(this)

        try {
            mOnlyMap = arguments?.getBoolean(ARGS_ONLY_MAP)!!
        } catch (e: Exception) {
            mOnlyMap = false
        }

        getMapAsync(onMapReadyCallback)
        if (!mOnlyMap) initApiClient()
    }

    override fun onStart() {
        mGoogleApiClient.connect()
        super.onStart()
    }

    override fun onStop() {
        mGoogleApiClient.disconnect()
        super.onStop()
    }

    override fun onDestroy() {
        mGoogleApiClient.disconnect()
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSION_LOCATION) {
            if (PermissionUtils.isAccessFineLocation(context)) {
                mMap?.isMyLocationEnabled = true
                reInitApiClient()
            }
        }
    }

    fun setIGoogleMapFragment(value: IMapFragment) {
        iMapFragment = value
    }

    private fun viewTrack(marks: ArrayList<TrackMark>) {
        val polyOptions = PolylineOptions()
        val latLngBounds = LatLngBounds.Builder()

        for (mark in marks) {
            val position = LatLng(mark.latitude as Double, mark.longitude as Double)
            polyOptions.add(position)
            latLngBounds.include(position)
        }

        mMap?.addPolyline(polyOptions)
        mMap?.setOnMapLoadedCallback {
            mMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 50))
        }

    }

    private fun initApiClient() {
        mGoogleApiClient.registerConnectionCallbacks(connectionCallback)
        mGoogleApiClient.registerConnectionFailedListener(connectionFailed)
    }

    private fun reInitApiClient() {
        mGoogleApiClient.disconnect()
        initApiClient()
        mGoogleApiClient.connect()
    }

    val onMapReadyCallback = OnMapReadyCallback { map ->
        mMap = map
        val marks = arguments?.getSerializable(ARGS_MARKS)

        if (marks != null) viewTrack(marks as ArrayList<TrackMark>)

        if (!mOnlyMap) {
            map?.uiSettings?.isZoomControlsEnabled = true
            map?.setOnMarkerDragListener(onMarkerDragListener)
            if (PermissionUtils.isAccessFineLocation(context)) {
                map?.isMyLocationEnabled = true
            } else {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_PERMISSION_LOCATION)
            }
        }

        iMapFragment?.onMapReady(map)
    }

    val connectionCallback = object : GoogleApiClient.ConnectionCallbacks {
        override fun onConnectionSuspended(p0: Int) {

        }

        override fun onConnected(p0: Bundle?) {
            val mLocationRequest = LocationRequest.create()
            mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            //TODO Интеравал снятия координат, стоит эти параметры добавить в настройки
            mLocationRequest.interval = 1000 //5000
            mLocationRequest.fastestInterval = 500 ///3000
            if (PermissionUtils.isAccessFineLocation(context)) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, locationListener)
            }
        }
    }

    var locationListener: LocationListener = LocationListener { location ->
        mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), 16f))
        iMapFragment?.locationListener(location)
    }

    val onMarkerDragListener = object : GoogleMap.OnMarkerDragListener {
        override fun onMarkerDragEnd(marker: Marker?) {
            iMapFragment?.onMarkerDragEnd(marker)
        }

        override fun onMarkerDrag(p0: Marker?) {
        }

        override fun onMarkerDragStart(p0: Marker?) {
        }

    }

    val connectionFailed = GoogleApiClient.OnConnectionFailedListener { connectionFailed ->
        Log.w(LOG_TAG, connectionFailed.errorMessage)
    }

    interface IMapFragment {
        fun onMapReady(map: GoogleMap)
        fun locationListener(location: Location)
        fun onMarkerDragEnd(p0: Marker?)
    }
}