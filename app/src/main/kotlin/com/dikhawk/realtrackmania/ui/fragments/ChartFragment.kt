package com.dikhawk.realtrackmania.ui.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dikhawk.realtrackmania.R
import lecho.lib.hellocharts.model.*
import lecho.lib.hellocharts.util.ChartUtils
import lecho.lib.hellocharts.view.LineChartView
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ChartFragment : Fragment() {

    private var mChartAltitude: LineChartView? = null


    companion object {
        private const val ARG_POINTS = "points"
        private const val ARG_NAME_AXIS_X = "name_axis_x"
        private const val ARG_NAME_AXIS_Y = "name_axis_y"

        fun newInstance(points: ArrayList<PointValue>, nameAxisX: String, nameAxisY: String): ChartFragment {
            val fragment = ChartFragment()
            val args = Bundle()

            args.putSerializable(ARG_POINTS, points)
            args.putString(ARG_NAME_AXIS_X, nameAxisX)
            args.putString(ARG_NAME_AXIS_Y, nameAxisY)

            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_chart_altitude, container, false)

        mChartAltitude = view.findViewById(R.id.chart_altitude) as LineChartView

//        mTrack = TracksDAO.getById(points)

        generateDate()

        return view
    }

    private fun generateDate() {
        var points: List<PointValue>? = arguments.getSerializable(ARG_POINTS) as ArrayList<PointValue>
        var nameAxisX: String? = arguments.getString(ARG_NAME_AXIS_X)
        var nameAxisY: String? = arguments.getString(ARG_NAME_AXIS_Y)

        var lineChartData: LineChartData?

        if (points != null) {
            val line = Line(points)
            line.color = ChartUtils.COLOR_BLUE
            line.shape = ValueShape.CIRCLE
            line.isCubic = false
            line.isFilled = false
            line.setHasLabels(false)
            line.setHasLabelsOnlyForSelected(false)
            line.setHasLines(true)
            line.setHasPoints(true)

            val lineList = ArrayList<Line>()
            lineList.add(line)
            lineChartData = LineChartData(lineList)

            val axisX = Axis()
            val axisY = Axis().setHasLines(true)

            axisX.name = nameAxisX
            axisY.name = nameAxisY

            lineChartData.axisXBottom = axisX
            lineChartData.axisYLeft = axisY
            lineChartData.baseValue = Float.NEGATIVE_INFINITY

            mChartAltitude?.lineChartData = lineChartData
        }
    }
}
