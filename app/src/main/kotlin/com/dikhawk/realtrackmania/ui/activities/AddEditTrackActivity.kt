package com.dikhawk.realtrackmania.ui.activities

import android.app.Activity
import android.content.res.ColorStateList
import android.location.Location
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.dikhawk.realtrackmania.R
import com.dikhawk.realtrackmania.data.db.TrackMarksDAO
import com.dikhawk.realtrackmania.data.db.TracksDAO
import com.dikhawk.realtrackmania.data.db.UsersDAO
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.dikhawk.realtrackmania.ui.fragments.GoogleMapFragment
import com.dikhawk.realtrackmania.ui.validators.EditTextValidator
import com.dikhawk.realtrackmania.utils.MapUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.google.maps.android.SphericalUtil
import java.util.*
import javax.inject.Inject

class AddEditTrackActivity : BaseActivity() {

    object INTENT_NAME {
        const val MODE = "mode"
    }

    object INTENT_VALUES {
        const val MODE_ADD = "mode_add"
        const val MODE_EDIT = "mode_edit"
    }

    private var mMap: GoogleMap? = null
    private var mCreateTrack: FloatingActionButton? = null
    private var mSave: FloatingActionButton? = null
    private var mClean: FloatingActionButton? = null
    private var mLastLocation: Location? = null
    private var mTrackLocationList = ArrayList<Location>()
    private var mTrackPolyLine: Polyline? = null
    private var trackingOn = false
    @Inject
    lateinit var usersDAO: UsersDAO
    @Inject
    lateinit var tracksDAO: TracksDAO
    @Inject
    lateinit var trackMarksDAO: TrackMarksDAO

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_track)
        displayHomeUp(true)

        initViews()

        val mapFragment = GoogleMapFragment.newInstance()
        mapFragment.setIGoogleMapFragment(iGoogleMapFragment)

        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.map_container, mapFragment)
        ft.commit()
    }


    private val iGoogleMapFragment = object : GoogleMapFragment.IMapFragment {
        override fun onMapReady(map: GoogleMap) {
            mMap = map
        }

        override fun locationListener(location: Location) {
            if (trackingOn) {
                val distance = getDistance(mLastLocation, location)
                if (distance > 3) { /// 3 is meters
                    mLastLocation = location
                    Toast.makeText(applicationContext, "Track point add. Size list: ${mTrackLocationList.size}",
                            Toast.LENGTH_LONG).show()

                    mTrackLocationList.add(location)
                }
            } else {
                mLastLocation = location
            }
        }

        override fun onMarkerDragEnd(p0: Marker?) {
            val points = mTrackPolyLine?.points
            val countMarker: Int = p0?.id?.substring(1)!!.toInt() //marker String name to Int, example: m1 to 1
            points?.set(countMarker, p0?.position)
            mTrackPolyLine?.points = points

            //TODO Перенести в активити гонка
            val isLocation = PolyUtil.isLocationOnPath(LatLng(mLastLocation?.latitude as Double,
                    mLastLocation?.longitude as Double), mTrackPolyLine?.points, true, 5.0)
            Log.w("LocationOnPath", isLocation.toString())
        }

    }

    private val createTrackBtnListener = View.OnClickListener { view ->
        if (trackingOn == false) {
            trackingOn = true
            mCreateTrack?.setImageResource(R.drawable.ic_stop)
        } else {
            trackingOn = false
            mCreateTrack?.setImageResource(R.drawable.ic_play)
            viewTrack()

            if (mTrackPolyLine != null) {
                mClean?.enableBtn()
                mSave?.enableBtn()
            }
        }
    }

    private val saveBtnListener = View.OnClickListener { view ->
        val dialogView = layoutInflater.inflate(R.layout.dialog_save_track, null)
        val saveDialog = AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle(R.string.title_save_track)
                .setPositiveButton(R.string.button_save, null)
                .setNegativeButton(R.string.button_cancel, { dialog, which -> dialog.dismiss() })
                .create()

        saveDialog.setOnShowListener { dialog ->
            val positiveBtn: Button = saveDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveBtn.setOnClickListener { view ->
                if (saveTrack(dialogView)) {
                    setResult(Activity.RESULT_OK)
                    saveDialog.dismiss()
                }
            }
        }

        saveDialog.show()
    }

    private val cleanBtnListener = View.OnClickListener { view ->
        mTrackPolyLine = null
        mTrackLocationList.clear()
        mMap?.clear()
        mClean?.disableBtn()
        mSave?.disableBtn()
    }

    private fun saveTrack(view: View): Boolean {
        val titleTrack = view.findViewById(R.id.et_title_track) as EditText
        val titleTrackIsValid = EditTextValidator.standardValidation(applicationContext, titleTrack)

        if (titleTrackIsValid) {
//            val ormLiteHelper = MyApplication.ormLiteHelper
            val user = usersDAO.getCurrentUser()
            val track = Track()
            val trackCoordinates = ArrayList<LatLng>(mTrackPolyLine?.points)
            val distance = SphericalUtil.computeLength(trackCoordinates)

            track.title = titleTrack.text.toString()
            track.user = user
            track.distance = distance
            track.maxAltitude = MapUtils.getMaxAltitude(mTrackLocationList)
            track.minAltitude = MapUtils.getMinAltitude(mTrackLocationList)

            tracksDAO.save(track)

            for (location in mTrackLocationList) {
                val mark = TrackMark()
                mark.latitude = location.latitude
                mark.longitude = location.longitude
                mark.altitude = location.altitude
                mark.track = track

                trackMarksDAO.save(mark)
            }

            mMap?.clear()
            mTrackPolyLine = null
            mTrackLocationList.clear()
            mClean?.disableBtn()
            mSave?.disableBtn()

            return true
        }

        return false
    }

    private fun viewTrack() {
        if (!mTrackLocationList.isEmpty()) {
            val polyOptions = MapUtils.locationListToPolylineOptions(mTrackLocationList)
            val latLngBounds = LatLngBounds.Builder()

            for (latLng in polyOptions.points) {
                mMap?.addMarker(MarkerOptions().position(latLng).draggable(true))
                latLngBounds.include(latLng)
            }

            mTrackPolyLine = mMap?.addPolyline(polyOptions)
            mMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), 50))
        }
    }

    private fun initViews() {
        mCreateTrack = findViewById(R.id.fab_create_track) as FloatingActionButton
        mSave = findViewById(R.id.fab_save) as FloatingActionButton
        mClean = findViewById(R.id.fab_clean) as FloatingActionButton

        mSave?.disableBtn()
        mClean?.disableBtn()

        mCreateTrack?.setOnClickListener(createTrackBtnListener)
        mSave?.setOnClickListener(saveBtnListener)
        mClean?.setOnClickListener(cleanBtnListener)
    }

    private fun FloatingActionButton.disableBtn() {
        val disableColor = ColorStateList.valueOf(
                ContextCompat.getColor(applicationContext, R.color.disabled))
        backgroundTintList = disableColor
        isClickable = false
        isEnabled = false
    }

    private fun FloatingActionButton.enableBtn() {
        val enableColor = ColorStateList.valueOf(
                ContextCompat.getColor(applicationContext, R.color.colorAccent))
        backgroundTintList = enableColor
        isClickable = true
        isEnabled = true
    }

    private fun getDistance(lastLocation: Location?, location: Location): Float {
        if (lastLocation == null) return 0F

        val result = floatArrayOf(0F)
        Location.distanceBetween(lastLocation.latitude, lastLocation.longitude,
                location.latitude, location.longitude, result)
        return result[0]
    }
}
