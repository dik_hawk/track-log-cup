package com.dikhawk.realtrackmania.data.db.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.dikhawk.realtrackmania.data.db.model.Track
import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.dikhawk.realtrackmania.data.db.model.TrackResult
import com.dikhawk.realtrackmania.data.db.model.User
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import java.sql.SQLException

/**
 * Created by dik on 29.06.16.
 */
class ORMLiteHelper(
        var context: Context) : OrmLiteSqliteOpenHelper(
        context, DATA_BASE_NAME, null, DATA_BASE_VERSION) {

    companion object {
        val DATA_BASE_NAME = "tracks.db"
        val DATA_BASE_VERSION = 1
    }

    val TAG = ORMLiteHelper::class.java.simpleName


    override fun onCreate(sqlDB: SQLiteDatabase?, cs: ConnectionSource?) {
        try {
            TableUtils.createTable(cs, Track::class.java)
            TableUtils.createTable(cs, User::class.java)
            TableUtils.createTable(cs, TrackMark::class.java)
            TableUtils.createTable(cs, TrackResult::class.java)
        } catch(e: SQLException) {
            Log.e(TAG, "Error creating tables: ${e.message}")
            throw RuntimeException(e)
        }
    }

    override fun onUpgrade(p0: SQLiteDatabase?, connectionSource: ConnectionSource?, p2: Int, p3: Int) {
//        try{
//            TableUtils.dropTable<TrackMark, Long>(connectionSource, TrackMark::class.java, true)
//            TableUtils.clearTable(connectionSource, Track::class.java)
//            TableUtils.clearTable(connectionSource, TrackResult::class.java)
//            TableUtils.clearTable(connectionSource, User::class.java)
//        } catch(e: SQLException) {
//            Log.e(TAG, "Error creating DB: ${e.message}")
//            throw RuntimeException(e)
//        }
    }
}