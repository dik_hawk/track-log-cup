package com.dikhawk.realtrackmania.data.db.model

import com.j256.ormlite.dao.ForeignCollection
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.field.ForeignCollectionField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable

/**
 * Created by dik on 13.05.16.
 */
@DatabaseTable(tableName = "users")
class User : Serializable {
    companion object COLUMN_NAME {
        const val ID = "id"
        const val SERVER_ID = "server_id"
        const val USER_NAME = "user_name"
    }

    @DatabaseField(generatedId = true, columnName = COLUMN_NAME.ID)
    var id: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.SERVER_ID)
    var serverId: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.USER_NAME)
    var userName: String? = null

    @ForeignCollectionField(eager = true)
    var tracks: ForeignCollection<Track>? = null
}