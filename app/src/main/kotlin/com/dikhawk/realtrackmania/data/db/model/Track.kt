package com.dikhawk.realtrackmania.data.db.model

import com.j256.ormlite.dao.ForeignCollection
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.field.ForeignCollectionField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable

/**
 * Created by dik on 13.05.16.
 */
@DatabaseTable(tableName = "tracks")
class Track : Serializable {

    companion object COLUMN_NAME {
        const val ID = "id"
        const val SERVER_ID = "server_id"
        const val TITLE = "title"
        const val MIN_ALTITUDE = "min_altitude"
        const val MAX_ALTITUDE = "max_altitude"
        const val DISTANCE = "distance"
    }


    @DatabaseField(generatedId = true, columnName = COLUMN_NAME.ID)
    var id: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.SERVER_ID)
    var serverId: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.TITLE)
    var title: String? = null

    @DatabaseField(columnName = COLUMN_NAME.MIN_ALTITUDE)
    var minAltitude: Double? = null

    @DatabaseField(columnName = COLUMN_NAME.MAX_ALTITUDE)
    var maxAltitude: Double? = null

    @DatabaseField(columnName = COLUMN_NAME.DISTANCE)
    var distance: Double? = null

    var userId: Long?
        get() = user?.id
        set(value) {
            userId = value
        }

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    var user: User? = null

    @ForeignCollectionField(eager = false)
    var trackResults: ForeignCollection<TrackResult>? = null

    @ForeignCollectionField(eager = false)
    var trackMarks: ForeignCollection<TrackMark>? = null
}