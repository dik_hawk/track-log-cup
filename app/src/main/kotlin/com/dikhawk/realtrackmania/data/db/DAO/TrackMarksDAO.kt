package com.dikhawk.realtrackmania.data.db

import com.dikhawk.realtrackmania.data.db.model.TrackMark
import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.support.ConnectionSource

/**
 * Created by dik on 13.05.16.
 */
class TrackMarksDAO(
        connectionSource: ConnectionSource?,
        dataClass: Class<TrackMark>?) : BaseDaoImpl<TrackMark, Long>(
        connectionSource, dataClass) {

    fun save(trackMark: TrackMark): Int {
        if (trackMark.id == null) {
            return create(trackMark)
        } else {
            return update(trackMark)
        }
    }
}