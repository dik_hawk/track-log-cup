package com.dikhawk.realtrackmania.data.db

import com.dikhawk.realtrackmania.data.db.model.Track
import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.support.ConnectionSource

/**
 * Created by dik on 13.05.16.
 */
class TracksDAO(
        connectionSource: ConnectionSource?,
        dataClass: Class<Track>?) : BaseDaoImpl<Track, Long>(
        connectionSource, dataClass) {

    fun save(track: Track): Int {
        if (track.id == null) {
            return create(track)
        } else {
            return update(track)
        }
    }

    fun getTracks(): List<Track> {
//        val queryBuilder: QueryBuilder<Track, Long> = queryBuilder()
//        queryBuilder.where().eq(Track.COLUMN_NAME.ID, id)
//
//        return query(queryBuilder.prepare()) as List<Track>
        return queryForAll()
    }

    fun getById(id: Long): Track {
//        val queryBuilder: QueryBuilder<Track, Long> = queryBuilder()
//        queryBuilder.where().eq(Track.COLUMN_NAME.ID, id)
//
//        return query(queryBuilder.prepare()) as Track
        return queryForId(id)
    }
}