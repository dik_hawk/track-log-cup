package com.dikhawk.realtrackmania.data.db

import com.dikhawk.realtrackmania.data.db.model.TrackResult
import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.support.ConnectionSource

/**
 * Created by dik on 13.05.16.
 */
class TrackResultsDAO(
        connectionSource: ConnectionSource?,
        dataClass: Class<TrackResult>?) : BaseDaoImpl<TrackResult, Long>(
        connectionSource, dataClass) {

}