package com.dikhawk.realtrackmania.data.db.model

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable

/**
 * Created by dik on 13.05.16.
 */
@DatabaseTable(tableName = "track_marks")
class TrackMark : Serializable {

    companion object COLUMN_NAME {
        const val ID = "id"
        const val SERVER_ID = "server_id"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val ALTITUDE = "altitude"
    }

    @DatabaseField(generatedId = true, columnName = COLUMN_NAME.ID)
    var id: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.SERVER_ID)
    var serverId: Long? = null

    @DatabaseField(columnName = COLUMN_NAME.LATITUDE)
    var latitude: Double? = null

    @DatabaseField(columnName = COLUMN_NAME.LONGITUDE)
    var longitude: Double? = null

    @DatabaseField(columnName = COLUMN_NAME.ALTITUDE)
    var altitude: Double? = null

    var trackId: Long?
        get() = track?.id
        set(value) {
            trackId = value
        }

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    var track: Track? = null
}