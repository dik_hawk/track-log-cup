package com.dikhawk.realtrackmania.data.db

import com.dikhawk.realtrackmania.data.db.model.User
import com.j256.ormlite.dao.BaseDaoImpl
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.support.ConnectionSource

/**
 * Created by dik on 13.05.16.
 */
class UsersDAO(
        connectionSource: ConnectionSource?,
        dataClass: Class<User>?) : BaseDaoImpl<User, Long>(
        connectionSource, dataClass) {

    fun save(user: User): Int {
        if (user.id == null) {
            return create(user)
        } else {
            return update(user)
        }
    }

    fun getCurrentUser(): User {
        val queryBuilder: QueryBuilder<User, Long> = queryBuilder()
        queryBuilder.where().eq(User.COLUMN_NAME.ID, 1)

        return queryForFirst(queryBuilder.prepare())
    }
}